function AverageScriptForce()
close all
times=[-5, 0, 0, 5, 10, 15, 20, 25];
figure(2);hold off; figure(1); hold off; figure(3); hold off;

%%measurement of wing disc 1 with collagenase
pathst='yourpath';%
times=[-5, 0, 2, 7, 12, 17, 22, 27, 32, 37, 42, 47, 52];
% list of folders that contains force indentation curves measured at different time points
paths={[pathst '-5min_processed/']; [pathst '0min_processed/'];...
    [pathst '0bismin_processed/']; [pathst '5min_processed/'];
    [pathst '10min_processed/']; [pathst '15min_processed/'];...
    [pathst '20min_processed/'];[pathst '25min_processed/'];
    [pathst '30min_processed/']; [pathst '35min_processed/'];
    [pathst '40min_processed/']; [pathst '45min_processed/'];
    [pathst '50min_processed/']; };
Fmax=350; 
xmin=-1200; xmax=500; %range of indentations


Colors=[[0.3 0 0.6]; [0 0 1]; [0 0.5 1];[0 1 0.5]; [0 1 0];[0.5 1 0];[0.5 0.5 0];[1 0 0]];
Colors=hsv(length(paths));
slopes=zeros(1,length(paths));
shifts=slopes;
LegendStrings=cell(length(paths),1);
newopts=optimset('lsqcurvefit');
optnew = optimset(newopts,'TolX',1e-18, 'MaxFunEvals',2800);
shift0=70; linfunc = @(p,x) p(1)*x+p(2); para0=[1 0];

for ind=1:length(paths)
    try
        temp=dir(char(paths(ind))); paths(ind)={[char(paths(ind)) temp(end).name '/']};
        files=dir([char(paths(ind)) '*.txt']);
        slopes2=zeros(1,length(files));
        shifts2=zeros(1,length(files));
        xvec=xmin:xmax; % admits to consider indentations down to -0.5 um
        ForceCurves2=zeros(length(files),length(xvec));
        for fileidx=1:length(files)
            ForceInd=importdata([char(paths(ind))  files(fileidx).name],' ',77);
            ForceIndData=ForceInd.data;
            ForceIndHeader=ForceInd.textdata(2:end,1);
            Kspring=str2num(ForceIndHeader{71}(19:25))*1000;%unit: pN/nm
            VertDefl=smooth(ForceIndData(:,2)*10^12,10); % force in pN
            temp=VertDefl;
            HeightMeas=ForceIndData(:,end-3)*10^9; % force in Nanometer
            HeightCorr=HeightMeas+0*VertDefl/Kspring; % factor 0 before VertDefl if cantilever bending was already taken into account in the analysis
            ForceCurves2(fileidx, :)=interp1(HeightCorr,VertDefl,xvec);
            yvec=ForceCurves2(fileidx, :);
        end
        
        temp=char(paths(ind,1));
        LegendStrings(ind)={[temp(length(pathst)+1:length(pathst)+6) ', N=' num2str(sum(~isnan(mean(ForceCurves2,2,'omitnan'))))] };
        
        yvec=mean(ForceCurves2,'omitnan'); % averaging of forces measured at one time point
        yvec=yvec-min(yvec); temp=yvec; yvec=yvec(temp<Fmax); xvec=xvec(temp<Fmax);
        
        [C,I]=min(yvec); 
        %xcon=xvec(min(find(yvec<min(yvec)+max(yvec)/10000)));%find rough estimate for contact point, option 1
        xcon=xvec(I)-20; %find rough estimate for contact point, option 2
        
        
        newopts=optimset('lsqcurvefit'); shift0=xcon;
        optnew = optimset(newopts,'TolX',1e-18, 'MaxFunEvals',2800);
        
        shift0=70;
        % least square fit for fine adjustment of the contact point 
        % a small shift of the rough guess xcon is calculated such that the force-indentation curve is best captured by a linear increase in the log-log representation
        [shift,resnorm,residual,exitflag,output,lambda,jacobian] =lsqnonlin(@CalcR2, ...
            shift0, [-350],[350], optnew, xvec, yvec, ind);
        shifts(ind)=shift;
        
        xvec=xvec-shift;
        figure(1); plot(xvec, yvec, 'Color', Colors(ind,:)); hold on;
        
        figure(2);
        loglog(-xvec,yvec,'o', 'MarkerEdgeColor', Colors(ind,:));
        hold on;
        ylim([10,500]); xlim([30,800])
        linfunc = @(p,x) p(1)*x+p(2); para0=[1 0];
        % least square fit, that finds the best power law exponent
        [para1,~,resid,~,~,~,jacob] = lsqcurvefit(linfunc,para0,log(-xvec(yvec>10 & xvec<0))/log(10),(log(yvec(yvec>10 & xvec<0)))/log(10));
        slopes(ind)=para1(1); % slope of force indentation curve measured at specific incubation time point
    catch
    end
end
plot(1:400, (1:400),'k', 'LineWidth',2)
plot(1:400, (1:400).^2/600,'k', 'LineWidth',2)
legend(LegendStrings);
savefig([pathst 'LogLogCurves_' pathst(end-15:end-1) '.fig']);

figure(1); legend(LegendStrings); savefig([pathst 'AllCurves_' pathst(end-15:end-1) '.fig']);

figure(3); yyaxis left; plot(times, slopes,'o');
yyaxis right; plot(times, shifts, 'o');
title(pathst);
savefig([pathst 'Slopes_' pathst(end-15:end-1) '.fig']);
end

function dist=CalcR2(shift, xvec, yvec, ind) % calculates the rsquared value
xvect=xvec-shift;
linfunc = @(p,x) p(1)*x-p(2); para0=[1 0];
[para1,~,resid,~,~,~,jacob] = lsqcurvefit(linfunc,para0,log(-xvect(yvec>10 & xvec<0)),log(yvec(yvec>10 & xvec<0)));
dist=(sum((linfunc(para1, log(-xvect(yvec>10 & xvec<0)))-log(yvec(yvec>10 & xvec<0))).^2)./sum((log(yvec(yvec>10 & xvec<0))-mean(log(yvec(yvec>10 & xvec<0)))).^2));
end

